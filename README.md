# Continuous Fuzzing for C/C++ Example

Fuzzing for C/C++ can help find both various complex and critical security. C/C++ are both memory unsafe languages were 
memory corruption bugs can lead to serious security vulnerabilities.

This tutorial focuses less on how to build libFuzzer targets and more on how to integrate the targets with GitLab. A lot of 
great information is available at the [libFuzzer](https://llvm.org/docs/LibFuzzer.html) tutorial.

This example will show the following steps:
* [Running the libFuzzer target via GitLab CI/CD](#running-fuzz-testing-from-CI)

Result:
* The libFuzzer targets will run continuously on the master branch .
* The libFuzzer targets will run regression tests on every pull-request (and every other branch)
 with the generated corpus and crashes to catch bugs early on.

### Understanding the bug

```text
extern "C" int LLVMFuzzerTestOneInput(const uint8_t * data, size_t size) {
      return size >= 3 &&
      data[0] == 'F' &&
      data[1] == 'U' &&
      data[2] == 'Z' &&
      data[3] == 'Z';
}

```

When `FUZZ` is passed to the program it triggeres an off-by-one memory violation access.

## Fuzzing
We will use the libFuzzer binary located under `fuzz/` to find the "secret" 
flag and trigger the bug.

```bash
./fuzz/fuzz_test_string -exact_artifact_path=crash
```

Because it's a very simple code libFuzzer takes under 1 second (Although even in complex code, libFuzzer can sometimes find complicated
 bugs quickly, such as Heartbleed).
 
 The output should look something like this:
 ```text
 INFO: Seed: 432631686
 INFO: Loaded 1 modules   (1 inline 8-bit counters): 1 [0x76bf40, 0x76bf41), 
 INFO: Loaded 1 PC tables (1 PCs): 1 [0x545248,0x545258), 
 INFO: -max_len is not provided; libFuzzer will not generate inputs larger than 4096 bytes
 INFO: A corpus is not provided, starting from an empty corpus
 #2      INITED cov: 1 ft: 2 corp: 1/1b lim: 4 exec/s: 0 rss: 26Mb
 =================================================================
 ==15057==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x603000009da4 at pc 0x0000005299ba bp 0x7fffddf93d80 sp 0x7fffddf93d78
 READ of size 1 at 0x603000009da4 thread T0
     #0 0x5299b9 in test_string(char const*, unsigned long) /home/jeka/fuzzitdev/continuous-fuzzing-example/src/test_string.cpp:15:17
     #1 0x5298f0 in LLVMFuzzerTestOneInput /home/jeka/fuzzitdev/continuous-fuzzing-example/fuzz/fuzz_test_string.cpp:13:5
     #2 0x42ef2a in fuzzer::Fuzzer::ExecuteCallback(unsigned char const*, unsigned long) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x42ef2a)
     #3 0x42e4d5 in fuzzer::Fuzzer::RunOne(unsigned char const*, unsigned long, bool, fuzzer::InputInfo*, bool*) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x42e4d5)
     #4 0x430a4e in fuzzer::Fuzzer::MutateAndTestOne() (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x430a4e)
     #5 0x431595 in fuzzer::Fuzzer::Loop(std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, fuzzer::fuzzer_allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > const&) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x431595)
     #6 0x424c53 in fuzzer::FuzzerDriver(int*, char***, int (*)(unsigned char const*, unsigned long)) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x424c53)
     #7 0x44d472 in main (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x44d472)
     #8 0x7fb0633e282f in __libc_start_main /build/glibc-LK5gWL/glibc-2.23/csu/../csu/libc-start.c:291
     #9 0x41d9c8 in _start (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x41d9c8)
 
 0x603000009da4 is located 0 bytes to the right of 20-byte region [0x603000009d90,0x603000009da4)
 allocated by thread T0 here:
     #0 0x526b52 in operator new[](unsigned long) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x526b52)
     #1 0x42ee11 in fuzzer::Fuzzer::ExecuteCallback(unsigned char const*, unsigned long) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x42ee11)
     #2 0x42e4d5 in fuzzer::Fuzzer::RunOne(unsigned char const*, unsigned long, bool, fuzzer::InputInfo*, bool*) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x42e4d5)
     #3 0x430a4e in fuzzer::Fuzzer::MutateAndTestOne() (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x430a4e)
     #4 0x431595 in fuzzer::Fuzzer::Loop(std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, fuzzer::fuzzer_allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > const&) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x431595)
     #5 0x424c53 in fuzzer::FuzzerDriver(int*, char***, int (*)(unsigned char const*, unsigned long)) (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x424c53)
     #6 0x44d472 in main (/home/jeka/fuzzitdev/continuous-fuzzing-example/build/fuzz/fuzz_test_string+0x44d472)
     #7 0x7fb0633e282f in __libc_start_main /build/glibc-LK5gWL/glibc-2.23/csu/../csu/libc-start.c:291
 
 SUMMARY: AddressSanitizer: heap-buffer-overflow /home/jeka/fuzzitdev/continuous-fuzzing-example/src/test_string.cpp:15:17 in test_string(char const*, unsigned long)
 Shadow bytes around the buggy address:
   0x0c067fff9360: fd fa fa fa fd fd fd fa fa fa fd fd fd fa fa fa
   0x0c067fff9370: fd fd fd fa fa fa fd fd fd fa fa fa fd fd fd fa
   0x0c067fff9380: fa fa fd fd fd fa fa fa fd fd fd fa fa fa fd fd
   0x0c067fff9390: fd fa fa fa fd fd fd fa fa fa fd fd fd fa fa fa
   0x0c067fff93a0: fd fd fd fa fa fa fd fd fd fa fa fa fd fd fd fa
 =>0x0c067fff93b0: fa fa 00 00[04]fa fa fa fa fa fa fa fa fa fa fa
   0x0c067fff93c0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
   0x0c067fff93d0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
   0x0c067fff93e0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
   0x0c067fff93f0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
   0x0c067fff9400: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
 Shadow byte legend (one shadow byte represents 8 application bytes):
   Addressable:           00
   Partially addressable: 01 02 03 04 05 06 07 
   Heap left redzone:       fa
   Freed heap region:       fd
   Stack left redzone:      f1
   Stack mid redzone:       f2
   Stack right redzone:     f3
   Stack after return:      f5
   Stack use after scope:   f8
   Global redzone:          f9
   Global init order:       f6
   Poisoned by user:        f7
   Container overflow:      fc
   Array cookie:            ac
   Intra object redzone:    bb
   ASan internal:           fe
   Left alloca redzone:     ca
   Right alloca redzone:    cb
   Shadow gap:              cc
 ==15057==ABORTING
 MS: 2 InsertRepeatedBytes-CMP- DE: "FUZZING IS IMPORTANT"-; base unit: adc83b19e793491b1c6ea0fd8b46cd9f32e592fc
 0x46,0x55,0x5a,0x5a,0x49,0x4e,0x47,0x20,0x49,0x53,0x20,0x49,0x4d,0x50,0x4f,0x52,0x54,0x41,0x4e,0x54,
 FUZZING IS IMPORTANT
 artifact_prefix='./'; Test unit written to crash
 Base64: RlVaWklORyBJUyBJTVBPUlRBTlQ=
 ```
 
 We can see clearly the heap-buffer-overflow READ 1 byte memory bug and the exact line
 that triggers it. We also see the data that triggered the bug, which is `FUZZING IS IMPORTANT`.
 
 The data is also saved to `./crash` and we can double test it with the main command line
 ```bash
 cat ./crash
 # FUZZING IS IMPORTANT
 ./src/test_string "FUZZING IS IMPORTANT"
 # you got it:)
 ```
 
 ## Running fuzz testing from CI
 
The best way to integrate libfuzzer fuzzing with Gitlab CI/CD is by adding additional stage & step to your `.gitlab-ci.yml`.

```yaml

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  script:
    - apt-get update -qq && apt-get install -y -qq git make clang cmake
    - export CC=`which clang`
    - export CXX=`which clang++`
    - mkdir -p build && cd build
    - cmake .. -DCMAKE_BUILD_TYPE=AddressSanitizer && make && cd ..
    - ./gitlab-cov-fuzz run --regression=$REGRESSION -- ./build/fuzz/fuzz_test_string
```

For each fuzz target you will have to create a step which extends `.fuzz_base` that runs the following:
* Builds the fuzz target
* Runs the fuzz target via `gitlab-cov-fuzz` CLI.
* For `$CI_DEFAULT_BRANCH` (can be override by `$COV_FUZZING_BRANCH`) will run fully fledged fuzzing sessions.
  For everything else including MRs will run fuzzing regression with the accumulated corpus and fixed crashes.  
 
 
