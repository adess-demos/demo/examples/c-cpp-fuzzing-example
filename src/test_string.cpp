#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//const char * FLAG = "FUZZING";

int test_string(const char *src, size_t len)
{
  return len >= 3 &&
      src[0] == 'F' &&
      src[1] == 'U' &&
      src[2] == 'Z' &&
      src[3] == 'Z';  // :‑<

}
